# Natuurcentrum frontend
This project is tightly coupled with the [natuurcentrum backend project](https://gitlab.com/datails/natuurcentrum-express-backend).

## Development
Make sure you clone both projects for end-to-end development.

Start the backend:

```bash
git clone https://gitlab.com/datails/natuurcentrum-express-backend
cd natuurcentrum-express-backend
docker-compose up -d
```

Start the front-end:

```bash
npm run start
```

If there are no users signed up yet, create some users in the mongo UI via: `localhost:8081`.

## Deployment
Deployment goes automatically to firebase.

```bash
firebase login:ci
firebase deploy --token "$FIREBASE_TOKEN" 
```