import * as React from "react";
import {
  Create,
  Edit,
  List,
  Datagrid,
  DateField,
  BooleanInput,
  BooleanField,
  ImageField,
  ImageInput,
  TextField,
  DeleteButton,
  EditButton,
  TextInput,
  SimpleForm,
  NumberInput,
  required,
  RichTextField,
  Show,
  SimpleShowLayout,
  SelectInput,
  ReferenceInput,
  ReferenceField,
  ArrayInput,
  SimpleFormIterator,
} from "react-admin";
import RichTextInput from "ra-input-rich-text";
import { Grid, makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  image: {
    height: "2rem",
    width: "2rem",
    borderRadius: "100%",
  },
});

export const ProductList = (props) => {
  const classes = useStyles();

  return (
    <List {...props}>
      <Datagrid>
        <TextField source="name" label="Productnaam" />
        <DateField
          source="date"
          showTime={true}
          disabled
          label="Laatst gewijzigd"
        />
        <ReferenceField
          link="show"
          label="Categorie"
          source="category"
          reference="categories"
        >
          <TextField source="name" />
        </ReferenceField>
        <ImageField
          classes={classes}
          source="pictures.src"
          label="Afbeelding"
        />
        <BooleanField
          source="harvestBeforeSummer"
          label="Oogst/bloei voor zomervakantie"
        />
        <BooleanField
          source="harvestAfterSummer"
          label="Oogst/bloei na zomervakantie"
        />
        <EditButton />
        <DeleteButton />
      </Datagrid>
    </List>
  );
};

export const ProductShow = (props) => {
  const classes = useStyles();

  return (
    <Grid container justify="center" spacing={2}>
      <Grid item xs={12} sm={12} md={4} xl={6} spacing={2}>
        <Show {...props} title="Product">
          <SimpleShowLayout>
            <DateField
              source="date"
              showTime={true}
              disabled
              label="Laatst gewijzigd"
            />
            <TextField source="name" label="Productnaam" />
            <ImageField
              classes={classes}
              source="pictures.src"
              label="Afbeelding"
            />
            <ReferenceField
              link="show"
              label="Categorie"
              source="category"
              reference="categories"
            >
              <TextField source="name" />
            </ReferenceField>
            <RichTextField source="description" label="Beschrijving" />
          </SimpleShowLayout>
        </Show>
      </Grid>
      <Grid item xs={12} sm={12} md={4} xl={6} spacing={2}>
        <Show {...props} title=" ">
          <SimpleShowLayout>
            <BooleanField
              source="harvestBeforeSummer"
              label="Oogst/bloei voor zomervakantie"
            />
            <BooleanField
              source="harvestAfterSummer"
              label="Oogst/bloei na zomervakantie"
            />
          </SimpleShowLayout>
        </Show>
      </Grid>
    </Grid>
  );
};

export const ProductEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source="name" label="Productnaam" validate={[required()]} />
      <ReferenceInput
        label="Categorie"
        source="category"
        reference="categories"
        link="show"
        validate={[required()]}
      >
        <SelectInput optionText="name" />
      </ReferenceInput>
      <RichTextInput source="description" label="Beschrijving" />
      <ImageInput
        maxSize={10000}
        source="pictures"
        label="Afbeelding"
        accept="image/*"
        placeholder={<p>Drop your file here. Max 10kb, else it won't work.</p>}
      >
        <ImageField source="src" />
      </ImageInput>
      <ArrayInput source="quantityTypes" label="Eenheidstypen">
        <SimpleFormIterator>
          <TextInput source="name" label="Eenheid" validate={[required()]} />
          <NumberInput source="quantity" label="Wegingsfactor" />
        </SimpleFormIterator>
      </ArrayInput>
      <BooleanInput
        source="harvestBeforeSummer"
        label="Oogst/bloei voor zomervakantie"
      />
      <BooleanInput
        source="harvestAfterSummer"
        label="Oogst/bloei na zomervakantie"
      />
      <BooleanInput source="isActive" label="Is actief" />
    </SimpleForm>
  </Edit>
);

export const ProductCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" validate={[required()]} label="Productnaam" />
      <ReferenceInput
        label="Categorie"
        source="category"
        reference="categories"
        link="show"
        validate={[required()]}
      >
        <SelectInput optionText="name" />
      </ReferenceInput>
      <RichTextInput source="description" label="Beschrijving" />
      <ImageInput
        maxSize={25000}
        source="pictures"
        label="Afbeelding"
        accept="image/*"
        placeholder={<p>Drop your file here. Max 10kb, else it won't work.</p>}
      >
        <ImageField source="src" />
      </ImageInput>
      <ArrayInput source="quantityTypes" label="Eenheidstypen">
        <SimpleFormIterator>
          <TextInput source="name" label="Eenheid" />
          <NumberInput source="quantity" label="Wegingsfactor" />
        </SimpleFormIterator>
      </ArrayInput>
      <BooleanInput
        source="harvestBeforeSummer"
        label="Oogst/bloei voor zomervakantie"
      />
      <BooleanInput
        source="harvestAfterSummer"
        label="Oogst/bloei na zomervakantie"
      />
      <BooleanInput source="isActive" lael="Is Actief" />
    </SimpleForm>
  </Create>
);
