import * as React from "react";
import {
  Create,
  Edit,
  List,
  Datagrid,
  BooleanInput,
  BooleanField,
  TextField,
  NumberField,
  DeleteButton,
  EditButton,
  TextInput,
  SimpleForm,
  NumberInput,
  required,
  Show,
  SimpleShowLayout,
  Filter,
  useTranslate,
} from "react-admin";
import { makeStyles, Chip } from "@material-ui/core";

const useQuickFilterStyles = makeStyles((theme) => ({
  chip: {
    marginBottom: theme.spacing(1),
  },
}));
const QuickFilter = ({ label }) => {
  const translate = useTranslate();
  const classes = useQuickFilterStyles();
  return <Chip className={classes.chip} label={translate(label)} />;
};

const PostFilter = (props) => (
  <Filter {...props}>
    <QuickFilter source="isActive" label="isActive" defaultValue={true} />
  </Filter>
);

export const ClientList = (props) => {
  return (
    <List {...props} filters={<PostFilter />}>
      <Datagrid>
        <TextField source="name" validate={[required()]} label="Naam" />
        <TextField source="zipcode" label="Postcode" />
        <TextField source="address" label="Straatnaam" />
        <NumberField source="housenumber" label="Huisnummer" />
        <BooleanField source="isActive" label="Is actief" />
        <EditButton />
        <DeleteButton />
      </Datagrid>
    </List>
  );
};

export const ClientShow = (props) => {
  return (
    <Show {...props} title="Organisatie">
      <SimpleShowLayout>
        <TextField source="name" label="Naam" />
        <TextField source="zipcode" label="Postcode" />
        <TextField source="address" label="Straatnaam" />
        <NumberField source="housenumber" label="Huisnummer" />
      </SimpleShowLayout>
    </Show>
  );
};

export const ClientEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source="name" validate={[required()]} label="Naam" />
      <TextInput source="zipcode" label="Postcode" />
      <TextInput source="address" label="Straatnaam" />
      <NumberInput source="housenumber" label="Huisnummer" />
      <BooleanInput source="isActive" label="Is actief" />
    </SimpleForm>
  </Edit>
);

export const ClientCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" validate={[required()]} label="Naam" />
      <TextInput source="zipcode" label="Postcode" />
      <TextInput source="address" label="Straatnaam" />
      <NumberInput source="housenumber" label="Huisnummer" />
      <BooleanInput source="isActive" label="Is actief" />
    </SimpleForm>
  </Create>
);
