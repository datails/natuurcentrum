import * as React from "react";
import {
  Create,
  Edit,
  List,
  Datagrid,
  BooleanInput,
  BooleanField,
  EmailField,
  PasswordInput,
  TextField,
  DeleteButton,
  EditButton,
  TextInput,
  SimpleForm,
  required,
  ReferenceField,
  ReferenceInput,
  SelectInput,
  Show,
  SimpleShowLayout,
  Filter,
  useTranslate,
} from "react-admin";
import { makeStyles, Chip } from "@material-ui/core";

const useQuickFilterStyles = makeStyles((theme) => ({
  chip: {
    marginBottom: theme.spacing(1),
  },
}));
const QuickFilter = ({ label }) => {
  const translate = useTranslate();
  const classes = useQuickFilterStyles();
  return <Chip className={classes.chip} label={translate(label)} />;
};

const PostFilter = (props) => (
  <Filter {...props}>
    <QuickFilter source="isAdmin" label="isAdmin" defaultValue={true} />
    <QuickFilter source="isActive" label="isActive" defaultValue={true} />
  </Filter>
);

export const UserList = (props) => {
  return (
    <List {...props} filters={<PostFilter />}>
      <Datagrid>
        <TextField source="name" label="Naam" />
        <EmailField
          source="email"
          label="Gebruikersnaam"
          style={{ color: "#3f51b5" }}
        />
        <ReferenceField
          link="show"
          label="Organisatie"
          source="client"
          reference="clients"
        >
          <TextField source="name" />
        </ReferenceField>
        <BooleanField source="isAdmin" />
        <BooleanField source="isActive" />
        <EditButton />
        <DeleteButton />
      </Datagrid>
    </List>
  );
};

export const UserShow = (props) => {
  return (
    <Show {...props}>
      <SimpleShowLayout>
        <TextField source="name" label="Naam" />
        <EmailField source="email" label="Gebruikersnaam" />
        <ReferenceField
          link="show"
          label="Organisatie"
          source="client"
          reference="clients"
        >
          <TextField source="name" />
        </ReferenceField>
      </SimpleShowLayout>
    </Show>
  );
};

export const UserEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source="name" label="Naam" validate={[required()]} />
      <ReferenceInput
        label="Organisatie"
        source="client"
        reference="clients"
        validate={[required()]}
      >
        <SelectInput optionText="name" />
      </ReferenceInput>
      <TextInput
        label="Gebruikersnaam"
        source="email"
        type="email"
        validate={[required()]}
      />
      <PasswordInput source="password" validate={[required()]} />
      <BooleanInput source="isActive" label="Is actief" />
      <BooleanInput source="isAdmin" label="Is administrator" />
    </SimpleForm>
  </Edit>
);

export const UserCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" validate={[required()]} />
      <ReferenceInput
        label="Organisatie"
        source="client"
        reference="clients"
        validate={[required()]}
        perPage={10000}
        allowEmpty
      >
        <SelectInput optionText="name" validate={[required()]} />
      </ReferenceInput>
      <TextInput
        label="Gebruikersnaam"
        source="email"
        type="email"
        validate={[required()]}
      />
      <PasswordInput source="password" validate={[required()]} />
      <BooleanInput source="isActive" label="Is actief" />
      <BooleanInput source="isAdmin" label="Is administrator" />
    </SimpleForm>
  </Create>
);
