import * as React from "react";
import {
  Show,
  TextField,
  ReferenceField,
  DateField,
  SimpleShowLayout,
  NumberField,
  ArrayField,
  Datagrid,
} from "react-admin";
import { Grid } from "@material-ui/core";

export default ({ permissions, ...props }) => {
  return (
    <Grid container justify="center" spacing={2}>
      <Grid item xs={12} sm={12} md={4} xl={6} spacing={2}>
        <Show {...props} title="Bestelling">
          <SimpleShowLayout>
            <TextField source="id" label="Order id" />
            <DateField
              source="orderDate"
              showTime={true}
              label="Aanvraag datum"
            />
            {/* <SelectField
              source="status"
              choices={[
                { id: "In behandeling", name: "In behandeling" },
                { id: "Geaccepteerd", name: "Geaccepteerd" },
                { id: "Geleverd", name: "Geleverd" },
              ]}
            /> */}
            <ReferenceField
              link="show"
              label="Geplaatst door"
              source="orderer"
              reference="users"
            >
              <TextField source="name" />
            </ReferenceField>
            <ReferenceField
              link="show"
              label="Organisatie"
              source="client"
              reference="clients"
            >
              <TextField source="name" />
            </ReferenceField>
          </SimpleShowLayout>
        </Show>
      </Grid>
      <Grid item xs={12} sm={12} md={8} xl={6}>
        <Show {...props} title=" ">
          <SimpleShowLayout>
            <NumberField
              source="price"
              label={"Prijs in euro's"}
              options={{ currency: "EUR" }}
            />
            <NumberField source="items" label={"Totaal aantal producten"} />
            <ArrayField source="products" label="Bestelling">
              <Datagrid>
                <ReferenceField
                  link="show"
                  label="Aantal"
                  source="id"
                  reference="products"
                >
                  <TextField source="name" />
                </ReferenceField>
                <NumberField source="price" />
                <NumberField source="items" />
                {/* <ReferenceArrayField label="Eenheid" reference="products" source="quantityTypes">
                  <SingleFieldList>
                      <TextField source="name" />
                  </SingleFieldList>
                </ReferenceArrayField> */}
                {/* <ReferenceField
                  link="show"
                  label="Eenheid"
                  source="quantityTypes"
                  reference="products"
                >
                  <TextField source="name" />
                </ReferenceField> */}
              </Datagrid>
            </ArrayField>
          </SimpleShowLayout>
        </Show>
      </Grid>
    </Grid>
  );
};
