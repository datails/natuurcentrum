import * as React from "react";
import introJs from "intro.js";
import {
  List,
  Datagrid,
  DateField,
  TextField,
  ShowButton,
  SelectField,
  CreateButton,
  TopToolbar,
  useListContext,
  sanitizeListRestProps,
} from "react-admin";
import decodeJwt from "jwt-decode";

let i = 0;

const ListActions = (props) => {
  const { className, exporter, filters, maxResults, ...rest } = props;
  const { basePath } = useListContext();
  return (
    <TopToolbar className={className} {...sanitizeListRestProps(rest)}>
      <CreateButton
        basePath={basePath}
        data-intro="Om een nieuwe bestelling te maken, ga je naar het bestelformulier."
        data-step="2"
        label="Nieuwe bestelling"
      />
    </TopToolbar>
  );
};

export default ({ permissions, ...props }) => {
  const role = decodeJwt(localStorage.getItem("token")).permissions;

  React.useEffect(() => {
    setTimeout(function () {
      if (localStorage.getItem("tutorial") !== "false" && i === 0) {
        i++;

        introJs()
          .setOptions({
            doneLabel: "Ik begrijp het",
            nextLabel: "Volgende",
            prevLabel: "Vorige",
            skipLabel: "Overslaan",
            exitOnEsc: true,
            keyboardNavigation: true,
            scrollToElement: true,
            exitOnOverlayClick: true,
            showProgress: true,
            hideNext: true,
            hidePrev: true,
            disableInteraction: true,
            overlayOpacity: 0.3,
          })
          .start();

        localStorage.setItem("tutorial", "false");
      }
    }, 4000);
  });

  return role === "admin" ? (
    <List {...props} title="Bestellingen">
      <Datagrid>
        <TextField source="id" label="Bestel id" />
        <DateField source="orderDate" showTime={true} label="Aanvraag datum" />
        {/* <SelectField
          source="status"
          choices={[
            { id: "In behandeling", name: "In behandeling" },
            { id: "Geaccepteerd", name: "Geaccepteerd" },
            { id: "Geleverd", name: "Geleverd" },
          ]}
        /> */}
        <ShowButton label="Bekijk bestelling" />
      </Datagrid>
    </List>
  ) : (
    <List
      {...props}
      title="Bestellingen"
      bulkActionButtons={false}
      actions={<ListActions />}
      data-intro="Een overzicht van alle bestellingen geplaatst door jullie school/buurttuin vind je hier."
      data-step="1"
    >
      <Datagrid>
        <TextField source="id" label="Bestel id" />
        <DateField source="orderDate" showTime={true} label="Aanvraag datum" />
        <SelectField
          source="status"
          choices={[
            { id: "In behandeling", name: "In behandeling" },
            { id: "Geaccepteerd", name: "Geaccepteerd" },
            { id: "Geleverd", name: "Geleverd" },
          ]}
        />
        <ShowButton />
      </Datagrid>
    </List>
  );
};
