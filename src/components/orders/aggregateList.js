export const createColumns = () => ([
  { key: "id", name: "ID" },
  { key: "items", name: "Items" },
  { key: "price", name: "Price" },
  { key: "name", name: "Product" },
])

export const aggregate = ({ products, orders }) => {
  const rows = [];

  orders.data.map(order => {
    order.products.forEach((product) => {
      if (rows.some(item => item.id === product.id)) {
        const obj = rows.find((item) => item.id === product.id);
        obj.items += product.items;
        obj.price += product.price;
      } else {
        rows.push({
          ...product,
          name: products.data.find((prod) => prod.id === product.id)?.name,
        });
      }
    });
  });

  return {
    columns: createColumns(),
    rows,
  };
};
