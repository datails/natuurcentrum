import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Drawer from "@material-ui/core/Drawer";
import { withStyles } from "@material-ui/core/styles";

import IconImageEye from "@material-ui/icons/RemoveRedEye";
import IconKeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import {
  Show,
  Button,
  SimpleShowLayout,
  TextField,
  RichTextField,
  ReferenceField,
  ImageField,
  BooleanField,
} from "react-admin";

const styles = (theme) => ({
  field: {
    // These styles will ensure our drawer don't fully cover our
    // application when teaser or title are very long
    "& *": {
      display: "inline-block",
      maxWidth: "20em",
    },
  },
});

const PostPreviewView = ({ classes, ...props }) => (
  <Show {...props} title="Product" style={{ "max-width": "400px" }}>
    <SimpleShowLayout>
      <TextField source="name" label="Productnaam" />
      <ImageField
        // classes={classes}
        source="pictures.src"
        label="Afbeelding"
      />
      <ReferenceField
        link="show"
        label="Categorie"
        source="category"
        reference="categories"
        linkType={false}
      >
        <TextField source="name" />
      </ReferenceField>
      <RichTextField source="description" label="Beschrijving" />
      <BooleanField
        source="harvestBeforeSummer"
        label="Oogst/bloei voor zomervakantie"
      />
      <BooleanField
        source="harvestAfterSummer"
        label="Oogst/bloei na zomervakantie"
      />
      <BooleanField source="leaf" label="blad" />
    </SimpleShowLayout>
  </Show>
);

const mapStateToProps = (state, props) => ({
  // Get the record by its id from the react-admin state.
  record: state.admin.resources[props.resource]
    ? state.admin.resources[props.resource].data[props.id]
    : null,
  version: state.admin.ui.viewVersion,
});

const PostPreview = connect(
  mapStateToProps,
  {}
)(withStyles(styles)(PostPreviewView));

class PostQuickPreviewButton extends Component {
  state = { showPanel: false };

  handleClick = () => {
    this.setState({ showPanel: true });
  };

  handleCloseClick = () => {
    this.setState({ showPanel: false });
  };

  render() {
    const { showPanel } = this.state;
    const { id } = this.props;

    return (
      <Fragment>
        <Button onClick={this.handleClick} label="Bekijk">
          <IconImageEye />
        </Button>
        <Drawer
          openSecondary={true}
          width={350}
          anchor="right"
          open={showPanel}
          onClose={this.handleCloseClick}
        >
          <div>
            <Button label="Sluiten" onClick={this.handleCloseClick}>
              <IconKeyboardArrowRight />
            </Button>
          </div>
          <PostPreview id={id} basePath="/products" resource="products" />
        </Drawer>
      </Fragment>
    );
  }
}

export default connect()(PostQuickPreviewButton);
