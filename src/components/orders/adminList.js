import * as React from "react";
import { Fragment } from "react";
import {
  List,
  Datagrid,
  TextField,
  NumberField,
  ReferenceField,
  BulkDeleteButton,
  Filter,
  useTranslate,
  useListContext,
  useDataProvider,
  Loading,
  ArrayField,
} from "react-admin";
import { makeStyles, Chip } from "@material-ui/core";

const useQuickFilterStyles = makeStyles((theme) => ({
  chip: {
    marginBottom: theme.spacing(1),
  },
}));
const QuickFilter = ({ label }) => {
  const translate = useTranslate();
  const classes = useQuickFilterStyles();
  return <Chip className={classes.chip} label={translate(label)} />;
};

const PostFilter = (props) => (
  <Filter {...props}>
    <QuickFilter source="isVerified" label="isVerified" defaultValue={true} />
  </Filter>
);

const PostBulkActionButtons = (props) => <></>;

const PostBulkActionButtonsAdmin = (props) => (
  <Fragment>
    <BulkDeleteButton {...props} />
  </Fragment>
);

const AdminGrid = () => {
  const { ids, data } = useListContext();
  const dataProvider = useDataProvider();

  const [products, setProducts] = React.useState();
  const [categories, setCategories] = React.useState();
  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    dataProvider
      .getList("products", {
        pagination: {},
        sort: {},
        filter: {},
      })
      .then((data) => {
        setProducts(data);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });

    dataProvider
      .getList("categories", {
        pagination: {},
        sort: {},
        filter: {},
      })
      .then((data) => {
        setCategories(data);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });
  }, []);

  if (loading) return <Loading />;
  if (!products || !categories) return null;

  return (
    <div style={{ margin: "1em" }}>
      {ids.map((id) => (
        <>
          <ArrayField source="products" record={data[id]}>
            <Datagrid>
              <ReferenceField
                record={data}
                link="show"
                label="Product"
                source="id"
                reference="products"
                basePath={"products"}
              >
                <TextField source="name" />
              </ReferenceField>
              <NumberField source="items" />
              <NumberField source="price" />
            </Datagrid>
          </ArrayField>
        </>
      ))}
    </div>
  );
};

export default ({ permissions, ...props }) => {
  return (
    <List
      {...props}
      filters={<PostFilter />}
      bulkActionButtons={
        permissions === "admin" ? (
          <PostBulkActionButtonsAdmin />
        ) : (
          <PostBulkActionButtons />
        )
      }
    >
      <AdminGrid />
    </List>
  );
};
