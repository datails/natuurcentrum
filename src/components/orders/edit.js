import * as React from "react";
import { useState, useEffect } from "react";
import {
  Edit,
  BooleanInput,
  SelectInput,
  TextField,
  SimpleForm,
  NumberInput,
  useDataProvider,
  Loading,
  Toolbar,
  SaveButton,
  useInput,
  ArrayInput,
  SimpleFormIterator,
  ReferenceInput,
  ReferenceField,
  FormDataConsumer,
  DeleteButton,
} from "react-admin";

const PostCreateToolbar = (props) => {
  const dataProvider = useDataProvider();
  const [products, setProducts] = useState();
  const [loading, setLoading] = useState(true);
  const inputs = useInput(props);

  useEffect(() => {
    dataProvider
      .getList("products", {
        pagination: {},
        sort: {},
        filter: {},
      })
      .then(({ data }) => {
        setProducts(data);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });
  }, []);

  if (loading) return <Loading />;
  if (!products) return null;

  const updatetProducts = [];

  for (const prod of inputs.input.value.products) {
    if (prod) {
      updatetProducts.push({
        ...prod,
        id: prod.id,
        price: products.find((item) => item.id === prod.id)?.price,
      });
    }
  }

  return (
    <Toolbar {...props}>
      <SaveButton
        submitOnEnter={true}
        transform={(data) => ({
          ...data,
          products: updatetProducts,
        })}
      />
    </Toolbar>
  );
};

export default ({ permissions, ...props }) => {
  // const classes = useStyles();
  // const token = localStorage.getItem("token");
  // const user = decodeJwt(token);

  const dataProvider = useDataProvider();
  const [products, setProducts] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    dataProvider
      .getList("products", {
        pagination: {},
        sort: {},
        filter: {},
      })
      .then(({ data }) => {
        setProducts(data.filter((item) => item.hidden));
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });
  }, []);

  if (loading) return <Loading />;
  if (!products) return null;

  return (
    <Edit {...props}>
      <SimpleForm toolbar={<PostCreateToolbar />}>
        <ReferenceField
          link="show"
          label="Orderer"
          source="orderer"
          reference="users"
        >
          <TextField source="name" />
        </ReferenceField>
        <ReferenceField
          link="show"
          label="Client"
          source="client"
          reference="clients"
        >
          <TextField source="name" />
        </ReferenceField>
        <FormDataConsumer>
          {({ formData, ...rest }) => {
            return (
              <ArrayInput source="products">
                {formData.isVerified && permissions !== "admin" ? (
                  <SimpleFormIterator disableAdd disableRemove>
                    <ReferenceInput
                      label="Product"
                      source="id"
                      reference="products"
                    >
                      <SelectInput optionText="name" disabled />
                    </ReferenceInput>
                    <NumberInput source="items" label="Aantal" disabled />
                    <ReferenceInput
                      label="Product"
                      source="id"
                      reference="products"
                    >
                      <SelectInput optionText="quantityTypes" />
                    </ReferenceInput>
                    <NumberInput
                      source="price"
                      label="Prijs per item"
                      disabled
                    />
                  </SimpleFormIterator>
                ) : (
                  <SimpleFormIterator>
                    <ReferenceInput
                      label="Product"
                      source="id"
                      reference="products"
                    >
                      <SelectInput optionText="name" />
                    </ReferenceInput>
                    <NumberInput source="items" label="Aantal" />
                    <NumberInput
                      source="price"
                      label="Prijs per item"
                      disabled
                    />
                  </SimpleFormIterator>
                )}
              </ArrayInput>
            );
          }}
        </FormDataConsumer>
        {permissions === "admin" && (
          <SelectInput
            source="category"
            choices={[
              { id: "In behandeling", name: "In behandeling" },
              { id: "Geaccepteerd", name: "Geaccepteerd" },
              { id: "Geleverd", name: "Geleverd" },
            ]}
          />
        )}
        {permissions === "admin" && <BooleanInput source="isVerified" />}
        {permissions === "admin" && <DeleteButton />}
      </SimpleForm>
    </Edit>
  );
};
