import * as React from "react";
import { useState, useEffect } from "react";
import {
  Create,
  TextInput,
  SimpleForm,
  NumberInput,
  required,
  useDataProvider,
  Loading,
  Toolbar,
  SaveButton,
  useInput,
  TextField,
  ReferenceField,
  SelectInput,
  FormDataConsumer
} from "react-admin";
import {
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  List,
  ListItem,
  ListItemText
} from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";
import decodeJwt from "jwt-decode";
import PostQuickPreviewButton from "./previewProduct";

const useStyles = makeStyles({
  image: {
    height: "2rem",
    width: "2rem",
    borderRadius: "100%",
  },
  width100: {
    width: "100%!important",
    margin: "25px 0 25px 0",
  },
  desc: {
    maxWidth: 250,
  },
  list: {
    display: 'flex',
    flexDirection: 'column',
    minWidth: 300,
    maxWidth: 1350,
    width: '100%'
  },
  hidden: {
    display: "none",
  },
  tableTitle: {
    margin: " 45px 0",
    fontSize: "1.5rem",
    color: "#000",
    display: "flex",
    paddingLeft: "15px",
  },
});

const persistFilledItems = ({ key, value, select, user }) => {
  const KEY = `filled:form:persistency${user.id}`;

  const oldState = JSON.parse(localStorage.getItem(KEY) || JSON.stringify({}))
  localStorage.setItem(KEY, JSON.stringify({
    ...oldState,
    [key]: {
      ...oldState[key],
      [select]: value
    }
  }))
}

const getInitialPersistentValue = ({ props, user }) => {
  const KEY = `filled:form:persistency${user.id}`;
  try {
    return JSON.parse(localStorage.getItem(KEY))[props.id].select;
  } catch {
    return false
  }
}

const PostCreateToolbar = (props) => {
  const dataProvider = useDataProvider();
  const [products, setProducts] = useState();
  const [loading, setLoading] = useState(true);
  const inputs = useInput(props);

  useEffect(() => {
    dataProvider
      .getList("products", {
        pagination: {},
        sort: {},
        filter: {},
      })
      .then(({ data }) => {
        setProducts(data);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });
  }, []);

  if (loading) return <Loading />;
  if (!products) return null;

  const updatetProducts = [];

  for (const i in inputs.input.value.products) {
    updatetProducts.push({
      ...inputs.input.value.products[i],
      id: i,
      price: products.find((item) => item.id === i).price,
    });
  }

  return (
    <Toolbar {...props}>
      <SaveButton
        label="Bestelling plaatsen"
        submitOnEnter={false}
        transform={(data) => ({
          ...data,
          products: updatetProducts,
        })}
      />
    </Toolbar>
  );
};

const CatalogueItem = ({ props, user }) => {
  const classes = useStyles();
  const [price, setPrice] = React.useState(0);

  return (
    <TableRow key={props.name}>
      <TableCell component="th" scope="row">
        <ReferenceField
          link="show"
          label="Product"
          source="id"
          reference="products"
          basePath={"/products"}
          linkType={false}
          record={props}
        >
          <TextField source="name" />
        </ReferenceField>
      </TableCell>
      <TableCell align="left">
        <PostQuickPreviewButton id={props.id} />
      </TableCell>
      <TableCell align="left">
        {props.harvestBeforeSummer ? <CheckIcon /> : ""}
      </TableCell>
      <TableCell align="left">
        {props.harvestAfterSummer ? <CheckIcon /> : ""}
      </TableCell>
      <TableCell align="left">
        {!getInitialPersistentValue({ props, user }) ? <SelectInput
            id={props.id}
            source={`products.${props.id}.quantityTypes`}
            choices={props.quantityTypes}
            label="Eenheid"
            onChange={(e) => {persistFilledItems({
              key: props.id,
              value: e.target.value,
              select: 'select',
              user
            })}
          }
        /> : <SelectInput
        id={props.id}
        source={`products.${props.id}.quantityTypes`}
        choices={props.quantityTypes}
        label="Eenheid"
        initialValue={`${getInitialPersistentValue({ props, user })}`}
        onChange={(e) => {persistFilledItems({
          key: props.id,
          value: e.target.value,
          select: 'select',
          user
        })}
      }
    />}
          
      </TableCell>
      <TableCell align="left">
      <FormDataConsumer>
        {({ formData, ...rest }) => formData?.products && formData?.products?.[props.id]?.quantityTypes &&
          <NumberInput
          onChange={(e) => { 
            persistFilledItems({
              key: props.id,
              value: e.target.value,
              select: 'input',
              user
            });
            
            return setPrice(e.target.value * (props.price || 0)) 
          }}
          label={"Hoeveelheid"}
          source={`products.${props.id}.items`}
          validate={[required(), (e) => e >= 0 ? 0: ['Mag geen negatief nummer zijn.'] ]}
          defaultValue={`${(() => {
            const KEY = `filled:form:persistency${user.id}`;
            try {
              return parseInt(JSON.parse(localStorage.getItem(KEY))[props.id].input, 10);
            } catch {

            }        
          
          })()}`}
          {...rest}
        />
        }
      </FormDataConsumer>
      </TableCell>
    </TableRow>
  );
};

function compare(a, b) {
  if (a === b) {
      return 0;
  }
  return a < b ? -1 : 1;
}

export default ({ permissions, ...props }) => {
  const classes = useStyles();
  const token = localStorage.getItem("token");
  const user = decodeJwt(token);

  const dataProvider = useDataProvider();
  const [products, setProducts] = useState([]);
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(true);

  const categoryOrder = {
    "5f74c718610ce0433e97adde" : 0, 
    "5f74c7a45a3e4f43968834d2": 1,
    "5f74c8015a3e4f43968834d3": 2,
    "5f9aca61c91e0900175b02db": 3,
    "5f74c8b65a3e4f43968834d5": 4,
    "5f74c9035a3e4f43968834d6": 5,
    "5f74c9125a3e4f43968834d7": 6
  }

  const categoryArr = [];

  for (const product of products) {
    if (
      categoryArr.some((arr) =>
        arr.some((prod) => prod.category === product.category)
      )
    ) {
      const arr = categoryArr.find((arr) =>
        arr.some((prod) => prod.category === product.category)
      );
      arr.push(product);
    } else {
      categoryArr.push([product]);
    }
  }

  // order bestellijst on id. 
  categoryArr.sort((a,b) => {

    const sortA = categoryOrder[categories.find(
      (cat) => cat.id === a.find((prop) => prop).category
    ).id]

    const sortB = categoryOrder[categories.find(
      (cat) => cat.id === b.find((prop) => prop).category
    ).id]

    const index = compare(sortA, sortB);

    return index

  })

  useEffect(() => {
    dataProvider
      .getList("products", {
        pagination: {},
        sort: {},
        filter: {},
      })
      .then(({ data }) => {
        setProducts(data.filter((item) => item.isActive));
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });

    dataProvider
      .getList("categories", {
        pagination: {},
        sort: {},
        filter: {},
      })
      .then(({ data }) => {
        setCategories(data);
      });
  }, []);

  if (loading) return <Loading />;
  if (!products || !categories) return null;

  return (
    <Create {...props} title="Plaats bestelling">
      <SimpleForm toolbar={<PostCreateToolbar />}>
        <Typography
          variant="h4"
          gutterBottom
          className={classes.width100}
          spacing={4}
        >
          Bestelformulier zaden en plantgoed
        </Typography>
        <Typography
          variant="subtitle1"
          gutterBottom
          className={classes.width100}
          spacing={4}
        >
          Leuk dat je moestuiniert op school of buurttuin!
          Natuurcentrum Arnhem helpt je graag, naast meedenken en advies,<br/>
          met het gratis leveren van de zaden en plantgoed die je voor jullie moestuin nodig hebt!
          In dit online bestelformulier kun je aangeven welke gewassen je wilt bestellen en hoeveel.<br/>
          Tips voor het invullen:
        </Typography>
        <List className={classes.list}>
          <ListItem>
            <ListItemText style={{width: '100%'}} primary={`1. Bedenk voorafgaand aan het invullen wat jij en collega’s of mede-tuinierders, allemaal willen bestellen en hoeveel. Het per mail toegestuurde formulier ‘Overzicht van te bestellen zaden en plantgoed’ kan je daarbij helpen, om het daar alvast op te schrijven. Een bestelling later veranderen of aanpassen is namelijk vrij moeilijk.`} />
          </ListItem>
          <ListItem>
            <ListItemText primary={`2. Er zijn zeven categorieën: groente, bloemen, kruiden die je elk als zaden of als plantgoed kan bestellen. De zevende categorie is ‘akker-zadenmengsel’.`} />
          </ListItem>
          <ListItem>
            <ListItemText primary={`3. Door aanvinken geef je aan wat je wilt hebben. Onder ‘eenheid’ selecteer je of je iets in gram, stuks, meter of vierkante meter wilt bestellen. Onder ‘hoeveelheid’ geef je de hoeveelheid daarvan aan.`} />
          </ListItem>
          <ListItem>
            <ListItemText primary={`4. Weet je zeker dat je alles hebt besteld wat je nodig hebt? Dan klik je op ‘bestelling plaatsen’. Je krijgt dan nog een overzichtje te zien wat je hebt besteld. Kopieer dat, plak het in een Word-document en sla dat voor jezelf op én stuur dat per mail naar mij (als back up). Ik ga het voor je bestellen!`} />
          </ListItem>
        </List>
        <Typography
          variant="subtitle1"
          gutterBottom
          className={classes.width100}
          spacing={4}
        >
          Voor vragen of advies:
          Natuurcentrum Arnhem, Geert Koning, adviseur Groene schoolpleinen, geert.koning@natuurcentrumarnhem.nl, 026-702 9016.
          Alvast veel plezier met deze voorbereiding!
        </Typography>
        <TextInput
          source="orderer"
          className={classes.hidden}
          defaultValue={user.id}
          disabled
          validate={[required()]}
        />
        <TextInput
          source="client"
          className={classes.hidden}
          defaultValue={user.client}
          disabled
          validate={[required()]}
        />
        {categoryArr.map((category) => {
          const color = categories.find(
            (cat) => cat.id === category.find((prop) => prop).category
          ).color;

          return (
            <>
              <ReferenceField
                link="show"
                label="Categorie"
                source="category"
                reference="categories"
                basePath={"/categories"}
                record={category.find((prop) => prop)}
                linkType={false}
              >
                <TextField source="name" className={classes.tableTitle} />
              </ReferenceField>
              <TableContainer className={classes.width100}>
                <Table
                  size="small"
                  aria-label="a dense table"
                  style={{
                    background: `${color}10`,
                    border: `.5px solid ${color}`,
                  }}
                >
                  <TableHead>
                    <TableRow>
                      <TableCell>Naam</TableCell>
                      <TableCell align="left">Bekijk product</TableCell>
                      <TableCell align="left">
                        Oogst/bloei voor zomervakantie
                      </TableCell>
                      <TableCell align="left">
                        Oogst/bloei na zomervakantie
                      </TableCell>
                      <TableCell align="left">Eenheid</TableCell>
                      <TableCell align="left">Gewenste hoeveelheid</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {category.sort().map((product) => (
                      <CatalogueItem props={product} user={user}/>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </>
          );
        })}
      </SimpleForm>
    </Create>
  );
};
