import list from "./list";
import show from "./show";
import create from "./create";
import edit from "./edit";

export const OrderCreate = create;
export const OrderShow = show;
export const OrderList = list;
export const OrderEdit = edit;
