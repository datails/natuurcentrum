import * as React from "react";
import {
  Create,
  Edit,
  List,
  Datagrid,
  RichTextField,
  TextField,
  TextInput,
  SimpleForm,
  required,
  Show,
  SimpleShowLayout,
  Filter,
  useTranslate,
  EditButton,
  DeleteButton,
} from "react-admin";
import RichTextInput from "ra-input-rich-text";
import { ColorField, ColorInput } from "react-admin-color-input";
import { makeStyles, Chip } from "@material-ui/core";

const useQuickFilterStyles = makeStyles((theme) => ({
  chip: {
    marginBottom: theme.spacing(1),
  },
}));
const QuickFilter = ({ label }) => {
  const translate = useTranslate();
  const classes = useQuickFilterStyles();
  return <Chip className={classes.chip} label={translate(label)} />;
};

const PostFilter = (props) => (
  <Filter {...props}>
    <QuickFilter source="isActive" label="isActive" defaultValue={true} />
  </Filter>
);

export const CategoryList = (props) => {
  return (
    <List {...props} filters={<PostFilter />}>
      <Datagrid>
        <TextField source="name" label="Naam" />
        <RichTextField source="description" label="Beschrijving" />
        <ColorField source="color" label="Kleur" picker="Swatches" />
        <EditButton />
        <DeleteButton />
      </Datagrid>
    </List>
  );
};

export const CategoryShow = (props) => {
  return (
    <Show {...props}>
      <SimpleShowLayout>
        <TextField source="name" label="Naam" />
        <RichTextField source="description" label="Beschrijving" />
        <ColorField source="color" label="Kleur" picker="Swatches" />
      </SimpleShowLayout>
    </Show>
  );
};

export const CategoryEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source="name" label="Naam" validate={[required()]} />
      <RichTextInput source="description" label="Beschrijving" />
      <ColorInput
        source="color"
        label="Kleur"
        validate={[required()]}
        picker="Swatches"
      />
    </SimpleForm>
  </Edit>
);

export const CategoryCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" label="Naam" validate={[required()]} />
      <RichTextInput source="description" label="Beschrijving" />
      <ColorInput
        source="color"
        label="Kleur"
        validate={[required()]}
        picker="Swatches"
      />
    </SimpleForm>
  </Create>
);
