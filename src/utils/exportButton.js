import * as React from 'react';
import { Button, makeStyles } from '@material-ui/core'
import ReactExport from "react-export-excel";
import GetAppIcon from '@material-ui/icons/GetApp';

const useStyles = makeStyles({
  button: {
    background: '#461e46 !important',
    color: '#FFF',
  },
});

const ExportButton = ({ rows, columns, total, all, categories }) => {

  const classes = useStyles()

  const ExcelFile = ReactExport.ExcelFile;
  const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
  const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

  const cols = [];
  const mappedCols = []

  // fill columns
  rows.forEach(row => row.columns.forEach( orderedProducts => orderedProducts.forEach(
    prd => {

      // get the current item
      const item = all[0].data.find(col => prd.id === col.id);


      // if the item already exist, skip it
      // as we can have multiple items, but they hold different quantity types
      if (!cols.includes(item.id)) {
        cols.push(item.id);
        mappedCols.push(item.name)

      }
    }
  )));

  const multiDataSet = [
    {
      columns: [
          'klant'
        ,
        ...mappedCols
      ],
      data: [
        ...rows.map( row => {
          return [
            all[2].data.find(client => client.id === row.client ).name,
            ...row.columns.map( orderedProducts => {
              const prods = orderedProducts.map(prd => {

                return prd.items ?? 0
              }) ?? [0];
            return prods.length ? prods : [0]
          }).flat()
          ]
      }),
      [
        "totaal",
        ...total.map( orderedProducts => {
          return orderedProducts.map(prd => {
            return prd.items
          })
        }).flat()
      ],
      [
        "Totaal gram",
        ...total.map( orderedProducts => {
          return orderedProducts.map(prd => {
            return prd.gram ? prd.gram.toFixed(2) : 0
          })
        }).flat(2)
      ]
    ],
    }
  ];

  return (
    <ExcelFile element={<Button className={classes.button}>Download Excel overzicht<GetAppIcon /></Button>} filename={`Zadenbestellijst${new Date().toISOString()}`}>
      <ExcelSheet name="totaal" dataSet={multiDataSet} />
      <ExcelSheet name="bestellingen" data={all[3].data}>
      <ExcelColumn label="id" value="id" />
        <ExcelColumn label="school/klant" value="client" />
        <ExcelColumn label="geplaatst door" value="orderer" />
        <ExcelColumn label="status" value="status" />
        <ExcelColumn label="geverifieerd" value={(col) => col.isVerified ? "Geverifieerd" : "Niet geverifieerd"}/>
        <ExcelColumn label="aantal items" value="items" />
        <ExcelColumn label="besteldatum" value="orderDate" />
        <ExcelColumn label="prijs" value="price" />
      </ExcelSheet>
      <ExcelSheet name="producten" data={all[0].data}>
        <ExcelColumn label="id" value="id" />
        <ExcelColumn label="naam" value="name" />
        <ExcelColumn label="status" value={(col) => col.isActive ? "Actief" : "Inactief"}/>
        <ExcelColumn label="categorieId" value="category" />
        <ExcelColumn label="laatste gewijzigd" value="date" />
        <ExcelColumn label="oogst/bloei na zomervakantie" value="harvestAfterSummer" />
        <ExcelColumn label="oogst/bloei voor zomervakantie" value="harvestBeforeSummer" />
        <ExcelColumn label="prijs" value="price" />
      </ExcelSheet>
      <ExcelSheet name="categorieen" data={categories.data}>
      <ExcelColumn label="id" value="id" />
        <ExcelColumn label="naam" value="name" />
        <ExcelColumn label="kleur" value="color" />
      </ExcelSheet>
      <ExcelSheet name="scholen" data={all[2].data}>
      <ExcelColumn label="id" value="id"/>
        <ExcelColumn label="naam" value="name"/>
        <ExcelColumn label="status" value={(col) => col.isActive ? "Actief" : "Inactief"}/>
        <ExcelColumn label="straatnaam"value="address"/>
        <ExcelColumn label="huisnummer"value="housenumber"/>
        <ExcelColumn label="postcode"value="zipcode"/>
      </ExcelSheet>
      <ExcelSheet name="gebruikers" data={all[1].data}>
        <ExcelColumn label="id" value="id" />
        <ExcelColumn label="naam" value="name" />
        <ExcelColumn label="status" value={(col) => col.isActive ? "Actief" : "Inactief"}/>
        <ExcelColumn label="rol" value={(col) => col.isAdmin ? "Administrator" : "editor"}/>
        <ExcelColumn label="wachtwoord" value="password" />
        <ExcelColumn label="gebruikersnaam" value="email" />
      </ExcelSheet>
    </ExcelFile>
  );
}

export default ExportButton