export const appConfig = {
  backendUri: process.env.NODE_ENV !== "production" ? process.env.REACT_APP_DEV_URI : process.env.REACT_APP_PRODUCTION_URI,
}