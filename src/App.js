import "dotenv/config";
import * as React from "react";
import { Admin, Resource } from "react-admin";
import UserIcon from "@material-ui/icons/People";
import BusinessIcon from "@material-ui/icons/Business";
import DashboardIcon from "@material-ui/icons/Dashboard";
import LocalOfferIcon from "@material-ui/icons/LocalOffer";
import TableChartIcon from "@material-ui/icons/TableChart";
import {
  ClientList,
  ClientEdit,
  ClientCreate,
  ClientShow,
} from "./components/clients/index";
import {
  ProductList,
  ProductEdit,
  ProductCreate,
  ProductShow,
} from "./components/products/index";
import {
  UserList,
  UserEdit,
  UserCreate,
  UserShow,
} from "./components/users/index";
import {
  OrderList,
  OrderCreate,
  OrderShow,
  OrderEdit,
} from "./components/orders/index";
import {
  CategoryList,
  CategoryCreate,
  CategoryShow,
  CategoryEdit,
} from "./components/categories/index";
import authProvider from "./framework/authProvider";
import dataProviders from "./dataProvider/index";
import Dashboard from "./layout/dashboard";
import Login from "./layout/login";
import Layout from "./layout/layout";

const App = () => (
  <Admin
    loginPage={Login}
    // dashboard={Dashboard}
    dataProvider={dataProviders}
    authProvider={authProvider}
    layout={Layout}
  >
    {(permissions) => [
      permissions === "admin" ? (
        <Resource
          name="dashboard"
          list={Dashboard}
          icon={DashboardIcon}
          options={{ label: "Overzicht" }}
        />
      ) : null,
      <Resource
        name="orders"
        basePath="orders"
        options={{ label: "Bestellingen" }}
        icon={TableChartIcon}
        list={OrderList}
        show={OrderShow}
        create={OrderCreate}
        edit={permissions === "admin" ? OrderEdit : null}
      />,
      <Resource
        options={{ label: "Categoriebeheer" }}
        name="categories"
        icon={LocalOfferIcon}
        list={permissions === "admin" ? CategoryList : null}
        show={CategoryShow}
        create={permissions === "admin" ? CategoryCreate : null}
        edit={permissions === "admin" ? CategoryEdit : null}
      />,
      <Resource
        options={{ label: "Productbeheer" }}
        name="products"
        list={permissions === "admin" ? ProductList : null}
        edit={permissions === "admin" ? ProductEdit : null}
        create={permissions === "admin" ? ProductCreate : null}
        show={ProductShow}
      />,
      <Resource
        options={{ label: "Scholenbeheer" }}
        icon={BusinessIcon}
        name="clients"
        list={permissions === "admin" ? ClientList : null}
        edit={permissions === "admin" ? ClientEdit : null}
        create={permissions === "admin" ? ClientCreate : null}
        show={ClientShow}
      />,
      <Resource
        options={{ label: "Gebruikersbeheer" }}
        icon={UserIcon}
        name="users"
        list={permissions === "admin" ? UserList : null}
        edit={permissions === "admin" ? UserEdit : null}
        create={permissions === "admin" ? UserCreate : null}
        show={UserShow}
      />,
    ]}
  </Admin>
);

export default App;
