import decodeJwt from "jwt-decode";
import { appConfig } from "../config/appConfig";

const authProvider = {
  login: async (params) => {
    const resp = await fetch(
      `${appConfig.backendUri}/login`,
      {
        method: "POST",
        body: JSON.stringify(params),
      }
    );

    if (resp.status < 200 || resp.status >= 300) {
      throw new Error(resp.statusText);
    }

    const { token } = await resp.json();
    localStorage.setItem("token", token);

    return Promise.resolve();
  },
  logout: () => {
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    return Promise.resolve();
  },
  checkError: (error) => {
    const status = error.status;
    if (status === 401 || status === 403) {
      return Promise.reject();
    }
    return Promise.resolve();
  },
  checkAuth: () =>
    localStorage.getItem("token") ? Promise.resolve() : Promise.reject(),
  getPermissions: () => {
    const role = decodeJwt(localStorage.getItem("token")).permissions;
    return role ? Promise.resolve(role) : Promise.reject();
  },
  getIdentity: () =>
    localStorage.getItem("user")
      ? Promise.resolve(decodeJwt(localStorage.getItem("user")))
      : Promise.reject(),
};

export default authProvider;
