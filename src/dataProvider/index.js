import productProvider from "./products/index";
import orderProvider from "./orders/index";
import { clientProvider, userProvider, categoryProvider } from './api'

const dataProviders = [
  { dataProvider: productProvider, resources: ["products"] },
  { dataProvider: clientProvider, resources: ["clients"] },
  { dataProvider: userProvider, resources: ["users"] },
  { dataProvider: orderProvider, resources: ["orders", "dashboard"] },
  { dataProvider: categoryProvider, resources: ["categories"] },
];

export default async (type, resource, params) => {
  const dataProviderMapping = dataProviders.find((dp) =>
    dp.resources.includes(resource)
  );
  return dataProviderMapping.dataProvider(type, resource, params);
};
