import { stringify } from "query-string";
import decodeJwt from "jwt-decode";
import { createHttpClient } from "../crud/utils";
import { appConfig } from "../../config/appConfig";
import { deleteMany, deleteOne, findOne, updateMany, updateOne } from "../crud/crud";

export const handleResponse = ({ json }) => ({
  data:
    json?.data?.map((record) => {
      const id = record._id;
      delete record._id;
      let price = 0;
      let items = 0;

      for (const product of record.products) {
        price += product.price * product.items;
        items += product.items;
      }

      return {
        ...record,
        id,
        price,
        items,
      };
    }) || [],
  total: json?.total,
})

export default async (type, resource, params) => {

  const token = localStorage.getItem("token");
  const decodedToken = decodeJwt(token);

  switch (type) {
    case "GET_LIST":
    case "GET_MANY_REFERENCES":
      const { page, perPage } = params.pagination;
      const { field, order } = params.sort;
      const query = {
        sort: JSON.stringify([field, order]),
        range: JSON.stringify([(page - 1) * perPage, perPage]),
        filter: JSON.stringify({
          ...params.filter,
          uuid:
            decodedToken.permissions !== "admin" ? decodedToken.client : "*",
        }),
      };

      return createHttpClient(`${appConfig.backendUri}/orders?${stringify(query)}`).then(handleResponse);

    case "GET_MANY":
      const qry = {
        filter: JSON.stringify({
          id: params.ids,
          uuid: resource === "orders" ? decodedToken.client : "*",
        }),
      };

      return createHttpClient(`${appConfig.backendUri}/orders?${stringify(qry)}`).then(handleResponse);

    case "GET_ONE":
      return findOne('order')(params)

    case "UPDATE":
      return updateOne('order')(params)

    case "UPDATE_MANY":
      return updateMany('orders')(params)

    case "DELETE":
      return deleteOne('orders')(params)

    case "DELETE_MANY":
      return deleteMany('orders')(params)

    case "CREATE":
      return createHttpClient(`${appConfig.backendUri}/order`, {
        method: "POST",
        body: JSON.stringify(params),
      }).then(({ json }) => {

        localStorage.removeItem(`filled:form:persistency${decodedToken.id}`)

        return {
          data: { ...params.data, id: json.data._id },
        }
      });

    default:
  }
};
