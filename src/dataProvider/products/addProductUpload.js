const convertFileToBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.readAsDataURL(file.rawFile);

    reader.onload = () => resolve(reader.result);
    reader.onerror = reject;
  });

export default (params) => {
  return convertFileToBase64(params.data.pictures).then((base64picture) => ({
    rawFile: params.data.pictures.rawFile,
    src: base64picture,
  }));
};
