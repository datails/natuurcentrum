import { stringify } from "query-string";
import { appConfig } from "../../config/appConfig";
import { deleteMany, deleteOne, updateMany } from "../crud/crud";
import { createHttpClient } from "../crud/utils";
import addProductUpload from "./addProductUpload";

export default async (type, resource, params) => {
  let base64image;

  if (params?.data?.pictures?.rawFile?.type) {
    base64image = await addProductUpload(params);
  }

  switch (type) {
    case "GET_LIST":
    case "GET_MANY_REFERENCES":
      const { page, perPage } = params.pagination;
      const { field, order } = params.sort;

      const query = {
        sort: JSON.stringify([field, order]),
        range: JSON.stringify([(page - 1) * perPage, perPage]),
        filter: JSON.stringify(params.filter),
      };

      return createHttpClient(`${appConfig.backendUri}/products?${stringify(query)}`).then(
        ({ json }) => ({
          data:
            json?.data?.map((record) => {
              // const id = record._id;
              // delete record._id;

              if (record.quantityTypes) {
                record.quantityTypes = record.quantityTypes.map((type) => {
                  return {
                    ...type,
                    id: type._id,
                  };
                });
              }

              return { id: record._id, ...record };
            }) || [],
          total: json?.total,
        })
      );

    case "GET_MANY":
      const qry = {
        filter: JSON.stringify({ id: params.ids }),
      };

      return createHttpClient(`${appConfig.backendUri}/products?${stringify(qry)}`).then(
        ({ json }) => ({
          data:
            json?.data?.map((record) => {
              // const id = record._id;
              // delete record._id;

              if (record.quantityTypes) {
                record.quantityTypes = record.quantityTypes.map((type) => {
                  return {
                    ...type,
                    id: type._id,
                  };
                });
              }

              return { id: record._id, ...record };
            }) || [],
          total: json?.total,
        })
      );

    case "GET_ONE":
      return createHttpClient(`${appConfig.backendUri}/product/${params.id}`).then(({ json }) => ({
        data: {
          ...json.data,
          quantityTypes: json.data?.map?.((type) => {
            return {
              ...type,
              id: type._id,
            };
          }),
        },
      }));

    case "UPDATE":
      return (async () => {
        return createHttpClient(`${appConfig.backendUri}/product/${params.id}`, {
          method: "PUT",
          body: JSON.stringify({
            data: {
              ...params.data,
              pictures: params.data?.pictures?.src
                ? {
                    ...params.previousData.pictures,
                    ...params.data.pictures,
                    ...base64image,
                  }
                : {},
            },
          }),
        }).then(({ json }) => ({ data: json }));
      })();

    case "UPDATE_MANY":
      return updateMany('products')(params)

    case "DELETE":
      return deleteOne('product')(params)

    case "DELETE_MANY":
      return deleteMany('products')(params)

    case "CREATE":
      return createHttpClient(`${appConfig.backendUri}/product`, {
        method: "POST",
        body: JSON.stringify({
          ...params,
          data: {
            ...params.data,
            pictures: {
              ...params.data.pictures,
              ...base64image,
            },
          },
        }),
      }).then(({ json }) => ({
        data: { ...params.data, id: json.data._id },
      }));

    default:
  }
};
