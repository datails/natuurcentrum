import { createCrudMap } from "./crud/crud";

export const categoryProvider = (type, _resource, params) => {
  return createCrudMap('category', 'categories').get(type)(params)
}

export const userProvider = (type, _resource, params) => {
  return createCrudMap('user', 'users').get(type)(params)
}

export const clientProvider = (type, _resource, params) => {
  return createCrudMap('client', 'clients').get(type)(params)
}