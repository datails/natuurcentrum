import { stringify } from "query-string";
import { appConfig } from '../../config/appConfig'
import { createHttpClient, createReferenceQuery, handleResponse, jsonToData } from './utils'

export const findManyReferences = routeName => params => createHttpClient(`${appConfig.backendUri}/${routeName}?${stringify(createReferenceQuery(params))}`).then(handleResponse);

export const findMany = routeName => params => {
  const query = {
    filter: JSON.stringify({ id: params.ids }),
  };

  return createHttpClient(`${appConfig.backendUri}/${routeName}?${stringify(query)}`).then(handleResponse);
}

export const findOne = routeName => params => 
  createHttpClient(`${appConfig.backendUri}/${routeName}/${params.id}`).then(jsonToData);

export const updateOne = routeName => params =>  
  createHttpClient(`${appConfig.backendUri}/${routeName}/${params.id}`, {
    method: "PUT",
    body: JSON.stringify(params),
  }).then(jsonToData)

export const updateMany = routeName => params =>
  createHttpClient(`${appConfig.backendUri}/${routeName}/${params.id}`, {
    method: "PUT",
    body: JSON.stringify(params),
  }).then(jsonToData)

export const deleteOne = routeName => params => 
  createHttpClient(`${appConfig.backendUri}/${routeName}/${params.id}`, {
    method: "DELETE",
  }).then(jsonToData)

export const deleteMany = routeName => params => { 
  const query = {
    filter: JSON.stringify({ id: params.ids }),
  };

  return createHttpClient(`${appConfig.backendUri}/${routeName}/${params.id}?${stringify(query)}`, {
    method: "DELETE",
    body: JSON.stringify(params),
  }).then(jsonToData)
}

export const createOne = routeName => params => 
  createHttpClient(`${appConfig.backendUri}/${routeName}`, {
    method: "POST",
    body: JSON.stringify(params),
  }).then(({ json }) => ({
    data: { ...params.data, id: json.data._id },
  }))

export const createCrudMap = (routeName, bulkRouteName) => {
  const bulkRoute = bulkRouteName || `${routeName}s`

  return new Map([
    ['GET_LIST', findManyReferences(bulkRoute)],
    ['GET_MANY_REFERENCES', findManyReferences(bulkRoute)],
    ['GET_MANY', findMany(bulkRoute)],
    ['GET_ONE', findOne(routeName)],
    ['UPDATE', updateOne(routeName)],
    ['UPDATE_MANY', updateMany(bulkRoute)],
    ['DELETE', deleteOne(routeName)],
    ['DELETE_MANY', deleteMany(bulkRoute)],
    ['CREATE', createOne(routeName)],
  ])
}