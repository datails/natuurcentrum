import { fetchUtils } from "react-admin";

export const createHttpClient = (url, options = {}) => {
  if (!options.headers) {
    options.headers = new Headers({ Accept: "application/json" });
  }

  options.headers.set("Authorization", `Bearer ${localStorage.getItem("token")}`);

  return fetchUtils.fetchJson(url, options);
};

export const normalizeResponse = ({ _id, ...record }) => ({ id: _id, ...record })

export const handleResponse = ({ json }) => ({
  data: json?.data?.map(normalizeResponse) || [],
  total: json?.total,
})

export const createReferenceQuery = (params) => {
  const { page, perPage } = params.pagination;
  const { field, order } = params.sort;

  return {
    sort: JSON.stringify([field, order]),
    range: JSON.stringify([(page - 1) * perPage, perPage]),
    filter: JSON.stringify(params.filter),
  };
}

export const jsonToData = ({ json: data }) => ({ data })