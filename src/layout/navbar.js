import * as React from "react";
import { useDispatch } from 'react-redux';
import {
  AppBar,
  IconButton,
  Toolbar,
  Tooltip,
  Typography,
  makeStyles
} from '@material-ui/core';
import { UserMenu, useTranslate, Logout } from "react-admin";
import MenuIcon from '@material-ui/icons/Menu';
import { toggleSidebar } from 'ra-core';
import { ReactComponent as Logo } from "../assets/logo.svg";
import Loader from './loader'
import HideOnScroll from './hideOnScroll';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const useStyles = makeStyles(
  theme => ({
      toolbar: {
          paddingRight: 24,
      },
      menuButton: {
          marginLeft: '0.5em',
          marginRight: '0.5em',
      },
      menuButtonIconClosed: {
          transition: theme.transitions.create(['transform'], {
              easing: theme.transitions.easing.sharp,
              duration: theme.transitions.duration.leavingScreen,
          }),
          transform: 'rotate(0deg)',
      },
      menuButtonIconOpen: {
          transition: theme.transitions.create(['transform'], {
              easing: theme.transitions.easing.sharp,
              duration: theme.transitions.duration.leavingScreen,
          }),
          transform: 'rotate(180deg)',
      },
      title: {
          flex: 1,
          textOverflow: 'ellipsis',
          whiteSpace: 'nowrap',
          overflow: 'hidden',
      },
      logo: {
        width: "50px",
      },
      navbar: {
        backgroundColor: "#461e46",
      },
      spacer: {
        flex: 1,
      },
  }),
  { name: 'RaAppBar' }
);

const CustomUserMenu = (props) => (
  <UserMenu {...props} logout={<Logout {...props} icon={<ExitToAppIcon/>} />}></UserMenu>
);

const CustomAppBar = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const translate = useTranslate();

  return (
    <HideOnScroll>
      <AppBar
        className={classes.navbar}
        {...props}
        elevation={1}
        userMenu={<CustomUserMenu />}
        
      >
        <Toolbar>
        <Tooltip
            title={translate(
                props.open
                    ? 'ra.action.close_menu'
                    : 'ra.action.open_menu',
                {
                    _: 'Open/Close menu',
                }
            )}
            enterDelay={500}
        >
            <IconButton
                color="inherit"
                onClick={() => dispatch(toggleSidebar())}
                className={classes.menuButton}
            >
                <MenuIcon
                    classes={{
                        root: props.open
                            ? classes.menuButtonIconOpen
                            : classes.menuButtonIconClosed,
                    }}
                />
            </IconButton>
        </Tooltip>
          <Typography
            variant="h6"
            color="inherit"
            className={classes.title}
            id="react-admin-title"
          />
          <Logo className={classes.logo} />
          <span className={classes.spacer} />
          <Loader />
          <CustomUserMenu />
        </Toolbar>

      </AppBar>
    </HideOnScroll>
  );
};

export default CustomAppBar;
