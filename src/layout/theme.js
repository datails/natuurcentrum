import { createMuiTheme } from "@material-ui/core";

const customTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#36454B",
      contrastText: "#fff",
    },
    secondary: {
      light: "#55dab3",
      main: "#00a883",
      dark: "#007856",
      contrastText: "#000",
    },
  },
  overrides: {
    RaLink: {
      root: {
        color: "#461e46",
      },
      active: {
        // borderLeft: '3px solid #4f3cc9',
      },
    },
    MuiFilledInput: {
      root: {
        backgroundColor: "rgba(0, 0, 0, 0.04)",
        "&$disabled": {
          backgroundColor: "rgba(0, 0, 0, 0.04)",
        },
      },
    },
  },
});

export default customTheme;
