import "date-fns";
import * as React from "react";
import { useState, useEffect } from "react";
import {
  useDataProvider,
  Loading,
  NumberField,
  TextField,
  List,
  ReferenceField,
  SelectField,
} from "react-admin";
import {
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableFooter,
  TableHead,
  TableRow,
  Typography,
  // Chip,
  Grid,
  // TextField,
} from "@material-ui/core";
import ExportButton from "../utils/exportButton"

const useStyles = makeStyles({
  image: {
    height: "2rem",
    width: "2rem",
    borderRadius: "100%",
  },
  table: {
    width: "100%!important",
  },
  desc: {
    maxWidth: 250,
  },
  hidden: {
    display: "none",
  },
  title: {
    margin: "15px 0",
  },
  noBorder: {
    borderBottom: "none",
    padding: 0,
  },
  bold: {
    fontWeight: 700,
  },
});

const getItemInfoOfAllOrders = (product, orders, productInfo) => {
  // get only those orders that has bought the product
  // we are seeking
  const ordersWithTheItem = orders?.filter?.((order) =>
    order?.products?.some?.((prod) => prod.id === product.id)
  );

  return ordersWithTheItem.reduce((acc, curr) => {
    const productInOrder = curr.products.find((prod) => prod.id === product.id);

    if (
      acc.some(
        (prodType) => prodType?.quantityTypes === productInOrder.quantityTypes
      )
    ) {
      const index = acc.findIndex(
        (prodType) => prodType.quantityTypes === productInOrder.quantityTypes
      );

      const cumulativeGram = acc[index].totalTypes.find(type => type.id === productInOrder.quantityTypes)?.quantity || 1;

      acc[index] = {
        ...acc[index],
        items: parseInt(acc[index].items) + parseInt(productInOrder.items),
        gram: cumulativeGram ? parseInt(acc[index].gram) + (cumulativeGram * parseInt(productInOrder.items)) : acc[index].gram,
      };

      return acc;
    } else {
      const currItem = curr?.products?.find((prod) => prod.id === product.id);
      const quantityType = productInfo.quantityTypes.find(type => type.id === currItem.quantityTypes);
      const gram = (quantityType?.quantity || 0) * currItem.items;

      acc.push({
        items:
          currItem.items || 0,
        gram,
        quantityTypes: curr?.products?.find((prod) => prod.id === product.id)
          ?.quantityTypes,
        totalTypes: productInfo.quantityTypes,
        id: product.id,
      });

      return acc;
    }
  }, []);
};

const buildColumns = (arr, categories, category) => {
  const items = arr.find((nestedArr) =>
    nestedArr.data.some((item) => item.orderer)
  );
  const data = items?.data;
  const columns = [];
  const rows = [];
  const total = [];

  if (!data) {
    return {
      columns,
      rows,
      total,
    };
  }

  const products = arr.find((nestedArr) =>
    nestedArr.data.some((item) => typeof item.harvestAfterSummer === "boolean")
  );

  // create column amount
  data.forEach((order, index) => {
    order.products.forEach((product) => {
      if (!columns.some((col) => col.id === product.id)) {
        const originCategory = products.data.find(
          (prd) => prd.id === product.id
        )?.category;

        const categoryName = categories.data.find(
          (cat) => cat.id === originCategory
        )?.name;

        if (originCategory === category) {
          columns.push(product);
        }
      }
    });
  });

  // create row amount
  data.forEach((order, index) => {
    let productIndex = rows.findIndex((row) => row.client === order.client);

    if (productIndex === -1) {
      rows.push({
        client: order.client,
        id: order.id,
        columns: [],
      });
    }
  });

  for (const row of rows) {
    // get all the orders that are of that client
    const linkedOrders = data.filter((order) => order.client === row.client);

    // create row amount including column length
    columns.forEach((col, index) => {

      // first get the productinfo because we need to know
      // which quantityTypes exist on the original product
      // to be able to make a mapping
      const productInfo = products?.data?.find?.((prod) => prod.id === col.id);

      // get the total amount of items per client
      const totalItems = getItemInfoOfAllOrders(col, linkedOrders, productInfo);

      // assign our totalItems to the column index
      row.columns[index] = totalItems;

      // if the total index already existed
      // we should add it to the existing one
      if (total[index]?.length) {
        // there can be multiple objects, for each
        // quantityType there should be 1 object
        for (const currentItem of totalItems) {

          // get the reference to the correct quantityType
          // and return the index
          const reference = total[index]?.findIndex?.(
            (item) => item.quantityTypes === currentItem.quantityTypes
          );

          const type = currentItem.totalTypes.find(type => type.id === currentItem.quantityTypes);

          // add our value
          total[index][reference === -1 ? 0 : reference ].items += parseInt(currentItem.items);
          total[index][reference === -1 ? 0 : reference ].gram += currentItem.gram;
        }
      } else {
        // if the item does not exist yet assign our nested array
        // to the index
        total[index] = JSON.parse(JSON.stringify(totalItems));
      }
    });
  }

  return {
    columns,
    rows,
    total,
  };
};

export default () => {
  const classes = useStyles();
  const dataProvider = useDataProvider();
  const [products, setProducts] = useState();
  const [categories, setCategories] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const promises = ["products", "users", "clients", "orders"].map(
      async (item) =>
        dataProvider.getList(item, {
          pagination: {},
          sort: {},
          filter: {},
        })
    );

    dataProvider
      .getList("categories", {
        pagination: {},
        sort: {},
        filter: {},
      })
      .then((data) => {
        setCategories(data);
      });

    Promise.all(promises)
      .then((data) => {
        setProducts(data);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });
  }, []);

  if (loading) return <Loading />;
  if (!products || !categories) return null;

  const props = {
    basePath: "/dashboard",
    hasCreate: false,
    hasEdit: false,
    hasList: true,
    hasShow: false,
    history: {},
    location: { pathname: "/", search: "", hash: "", state: undefined },
    match: { path: "/", url: "/", isExact: true, params: {} },
    options: {},
    permissions: null,
    resource: "orders",
  };

  return (
    <Grid container justify="center" spacing={2}>
       
      {categories.data.map((category) => {
        const { columns, rows, total } = buildColumns(
          products,
          categories,
          category.id
        );

        if (columns.length === 0) return null;

        return (
          <>
          <Grid item xs={12} sm={12} md={12} xl={12} spacing={2}>
              <Typography variant="h6" gutterBottom className={classes.title}>
                {category.name}
              </Typography>

            <List
              {...props}
              actions={false}
              title=" "
            >
              <TableContainer className={classes.table} spacing={2}>
                <Table
                  // className={classes.table}
                  size="small"
                  aria-label="dashboard"
                >
                  <TableHead>
                    <TableRow>
                      <TableCell align="left" scope="row">
                        Klant
                      </TableCell>
                      {columns.map((column) => {
                        return (
                          <TableCell align="left" scope="row">
                            <ReferenceField
                              record={column}
                              link="show"
                              label="Product"
                              source="id"
                              reference="products"
                              basePath={"products"}
                            >
                              <TextField source="name" />
                            </ReferenceField>
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows.map((row) => {
                      return (
                        <TableRow>
                          <TableCell align="left" scope="row">
                            <ReferenceField
                              record={row}
                              link="show"
                              label="Klant"
                              source="client"
                              reference="clients"
                              basePath={"clients"}
                            >
                              <TextField source="name" />
                            </ReferenceField>
                          </TableCell>
                          {row.columns.map((val) => {
                            if (val === "client") {
                              return null;
                            }
                            return (
                              <TableCell>
                                <TableContainer className={classes.table}>
                                  <Table size="small">
                                    <TableBody>
                                      <TableRow>
                                        {val.map((itemType) => (
                                          <TableCell
                                            align="left"
                                            scope="row"
                                            className={classes.noBorder}
                                          >
                                            <b>
                                              <SelectField
                                                source="quantityTypes"
                                                record={itemType}
                                                choices={itemType.totalTypes}
                                                label="Eenheid"
                                                className={classes.bold}
                                              />
                                              :{" "}
                                            </b>
                                            <i>
                                              <NumberField
                                                source="items"
                                                record={itemType}
                                              />
                                              {itemType.gram ?
                                          ` (${itemType.gram.toFixed(2)} gram)`
                                          : ''
                                          }
                                            </i>
                                          </TableCell>
                                        ))}
                                      </TableRow>
                                    </TableBody>
                                  </Table>
                                </TableContainer>
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })}
                  </TableBody>
                  <TableFooter>
                    <TableRow>
                      <TableCell>Totaal</TableCell>
                      {total.map((val) => (
                        <TableCell>
                          <TableContainer className={classes.table}>
                            <Table size="small">
                              <TableBody>
                                <TableRow>
                                  {val.map((itemType) => (
                                    <TableCell
                                      align="left"
                                      scope="row"
                                      className={classes.noBorder}
                                    >
                                      <b>
                                        <SelectField
                                          source="quantityTypes"
                                          record={itemType}
                                          choices={itemType.totalTypes}
                                          label="Eenheid"
                                          className={classes.bold}
                                        />
                                        :{" "}
                                      </b>
                                      <i>
                                        <NumberField
                                          source="items"
                                          record={itemType}
                                        />
                                          {itemType.gram ?
                                          ` (${itemType.gram.toFixed(2)} gram)`
                                          : ''
                                          }
                                      </i>
                                    </TableCell>
                                  ))}
                                </TableRow>
                              </TableBody>
                            </Table>
                          </TableContainer>
                        </TableCell>
                      ))}
                    </TableRow>
                  </TableFooter>
                </Table>
              </TableContainer>
            </List>
          </Grid>
          <ExportButton rows={rows} columns={columns} total={total} all={products} categories={categories}/>
        </>);
      })}
    </Grid>
  );
};
