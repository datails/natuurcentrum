import React from "react";
import {
  createMuiTheme,
  createStyles,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import { orange } from "@material-ui/core/colors";
import { Grid, Link, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) =>
  createStyles({
    link: {
      color: "#fff",
    },
    container: {
      background: "#461e46",
      margin: "0",
      width: "100%",
      padding: "10px",
      heigth: "35px",
    },
    margin: {
      margin: theme.spacing(1),
    },
    noPadding: {
      padding: "2px 0 0 0 !important",
    },
  })
);

const theme = createMuiTheme({
  palette: {
    primary: orange,
  },
});

export default function Footer() {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <Grid
        container
        spacing={10}
        className={classes.container}
        justify="center"
      >
        <Grid
          className={classes.noPadding}
          item
          xs={12}
          sm={12}
          md={4}
          justify="center"
          alignItems="center"
        >
          <Typography
            variant="subtitle1"
            component="h2"
            gutterBottom
            align="center"
          >
            <Link
              className={classes.link}
              href={"https://datails.nl"}
              target="_blank"
            >
              Powered by Datails
            </Link>
          </Typography>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
